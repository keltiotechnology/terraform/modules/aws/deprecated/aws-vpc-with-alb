## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.0 |
| <a name="requirement_null"></a> [null](#requirement\_null) | >= 2.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_subnets"></a> [subnets](#module\_subnets) | cloudposse/dynamic-subnets/aws | 0.39.3 |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | cloudposse/vpc/aws | 0.26.1 |

## Resources

| Name | Type |
|------|------|
| [aws_lb.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_application_balancer_name"></a> [application\_balancer\_name](#input\_application\_balancer\_name) | Name of the application balancer | `string` | n/a | yes |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | List of availability zones | `list(string)` | n/a | yes |
| <a name="input_aws_profile"></a> [aws\_profile](#input\_aws\_profile) | AWS credential profile | `string` | `"default"` | no |
| <a name="input_aws_security_group_load_balancer_id"></a> [aws\_security\_group\_load\_balancer\_id](#input\_aws\_security\_group\_load\_balancer\_id) | ID of security group for the application load balancer | `string` | n/a | yes |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name or abbreviation, e.g. 'eg' or 'cp' | `string` | n/a | yes |
| <a name="input_vpc_cidr_block"></a> [vpc\_cidr\_block](#input\_vpc\_cidr\_block) | VPC CIDR block | `string` | n/a | yes |
| <a name="input_vpc_name"></a> [vpc\_name](#input\_vpc\_name) | Solution name, e.g. 'app' or 'jenkins' | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_application_load_balancer_arn"></a> [application\_load\_balancer\_arn](#output\_application\_load\_balancer\_arn) | n/a |
| <a name="output_application_load_balancer_dns_name"></a> [application\_load\_balancer\_dns\_name](#output\_application\_load\_balancer\_dns\_name) | n/a |
| <a name="output_application_load_balancer_zone_id"></a> [application\_load\_balancer\_zone\_id](#output\_application\_load\_balancer\_zone\_id) | n/a |
| <a name="output_aws_lb_arn"></a> [aws\_lb\_arn](#output\_aws\_lb\_arn) | n/a |
| <a name="output_aws_lb_dns_name"></a> [aws\_lb\_dns\_name](#output\_aws\_lb\_dns\_name) | n/a |
| <a name="output_aws_lb_zone_id"></a> [aws\_lb\_zone\_id](#output\_aws\_lb\_zone\_id) | n/a |
| <a name="output_private_subnet_ids"></a> [private\_subnet\_ids](#output\_private\_subnet\_ids) | The private subnet ids of VPC |
| <a name="output_public_subnet_ids"></a> [public\_subnet\_ids](#output\_public\_subnet\_ids) | The public subnet ids of VPC |
| <a name="output_vpc_cidr_block"></a> [vpc\_cidr\_block](#output\_vpc\_cidr\_block) | VPC cidr block |
| <a name="output_vpc_default_security_group_id"></a> [vpc\_default\_security\_group\_id](#output\_vpc\_default\_security\_group\_id) | The ID of the security group created by default on VPC creation |
| <a name="output_vpc_id"></a> [vpc\_id](#output\_vpc\_id) | ID of VPC |

## Usage
```hcl-terraform
module "vpc" {
    source = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-vpc-with-alb"
    namespace = "<Namespace>"
    vpc_name = "<VPC name>"
    vpc_cidr_block = "<CIDR Block e.g. 172.16.0.0/16>"
    availability_zones = "<A list available zones>"
    application_balancer_name = "<Application balancer name>"
    aws_security_group_load_balancer_id = "<Security group ID for the application load balancer>"
}
```
