module "vpc" {
  source     = "cloudposse/vpc/aws"
  version    = "0.26.1"
  cidr_block = var.vpc_cidr_block
  namespace  = var.namespace
  name       = var.vpc_name
  security_group_enabled = false
}

module "subnets" {
  source              = "cloudposse/dynamic-subnets/aws"
  version             = "0.39.3"
  availability_zones  = var.availability_zones
  vpc_id              = module.vpc.vpc_id
  igw_id              = module.vpc.igw_id
  cidr_block          = module.vpc.vpc_cidr_block
  nat_gateway_enabled = true

  depends_on = [
    module.vpc
  ]
}

resource "aws_lb" "default" {
  name               = var.application_balancer_name
  load_balancer_type = "application"
  internal           = false
  security_groups    = [var.aws_security_group_load_balancer_id]
  subnets            =  module.subnets.public_subnet_ids
}
